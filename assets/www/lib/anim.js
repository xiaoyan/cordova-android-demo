function startAnim(widget, _anim) {
	var anim = 'animated ' + _anim;
	widget.addClass(anim);
	var wait = window.setTimeout(function() {
		widget.removeClass(anim);
	}, 1300);
}